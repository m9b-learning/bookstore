import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { AdminComponent } from './admin.component';
import { NewBookComponent } from './new-book/new-book.component';
import { NewAuthorComponent } from './new-author/new-author.component';
import { adminRoutes } from './admin.routes';
import { ToolsModule } from '../tools/tools.module';

@NgModule({
  declarations: [
    AdminComponent,
    NewBookComponent,
    NewAuthorComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(adminRoutes),
    ToolsModule
  ]
})
export class AdminModule { }
