import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthorService } from 'src/app/services/author.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Author } from 'src/app/models/author.model';

@Component({
  selector: 'app-new-author',
  templateUrl: './new-author.component.html',
  styleUrls: ['./new-author.component.scss']
})
export class NewAuthorComponent implements OnInit {

  submitted = false;
  error: string;

  authorForm = new FormGroup({
    name: new FormControl('', [
      Validators.required,
      Validators.minLength(2)
    ])
  });

  get name() { return this.authorForm.get('name'); }

  constructor(
    private authorService: AuthorService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
  }

  onSubmit() {
    this.submitted = true;
    if (this.authorForm.valid) {
      const form = this.authorForm.value;
      this.authorService.createAuthor(form).subscribe(
        (author: Author) => { 
          console.log('Created author:', author);
          this.router.navigate(['..'], {
            relativeTo: this.route
          });
        },
        (error) => { this.error = error.message; }
      );
    }
  }
}
