import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Book } from 'src/app/models/book.model';
import { BookService } from 'src/app/services/book.service';

@Component({
  selector: 'app-new-book',
  templateUrl: './new-book.component.html',
  styleUrls: ['./new-book.component.scss']
})
export class NewBookComponent implements OnInit {

  book = new Book();
  submitted = false;
  error: string;

  constructor(
    private bookService: BookService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
  }

  onSubmit(form) {
    this.submitted = true;
    if (form.valid) {
      this.bookService.createBook(this.book).subscribe(
        (book: Book) => { 
          console.log('Created book:', book);
          this.router.navigate(['..'], {
            relativeTo: this.route
          });
        },
        (error) => { this.error = error.message; }
      );
    }
  } 
}
