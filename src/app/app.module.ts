import { BrowserModule } from '@angular/platform-browser';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { AppComponent } from './app.component';
import { BooksModule } from './books/books.module';
import { ToolsModule } from './tools/tools.module';
import { LogInterceptorService } from './services/log-interceptor.service';
import { appRoutes } from './app.routes';
import { translateOptions } from './app.translate';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BooksModule,
    ToolsModule,
    RouterModule.forRoot(appRoutes),
    TranslateModule.forRoot(translateOptions)
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: LogInterceptorService, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
