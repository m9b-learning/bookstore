import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';

import { BookDetailComponent } from './book-detail.component';
import { BookService } from 'src/app/services/book.service';
import { TestModule } from 'src/app/test/test.module';

const book = {
  id: 0,
  title: 'Title 1',
  author: 'Author 1',
  resume: 'Resume'
};

describe('BookDetailComponent', () => {
  let component: BookDetailComponent;
  let fixture: ComponentFixture<BookDetailComponent>;

  const mockBookService = jasmine.createSpyObj('BookService', {
    getBook: of(book)
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookDetailComponent ],
      providers: [
        { provide: BookService, useValue: mockBookService }
      ],
      imports: [ TestModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display book title', () => {
    const el = fixture.nativeElement;
    expect(el.querySelector('.tu-title').textContent).toContain(book.title);
  });

  it('should display book author', () => {
    const el = fixture.nativeElement;
    expect(el.querySelector('.tu-author').textContent).toContain(book.author);
  });

  it('should display book resume', () => {
    const el = fixture.nativeElement;
    expect(el.querySelector('.tu-resume').textContent).toContain(book.resume);
  });
});
