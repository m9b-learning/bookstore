import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { BookService } from 'src/app/services/book.service';
import { Book } from 'src/app/models/book.model';

@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.scss']
})
export class BookDetailComponent implements OnInit {

  book: Book;
  error: string;
  
  constructor(
    private bookService: BookService,
    private route: ActivatedRoute
  ) {
    const id = +(this.route.snapshot.params.id);
    this.bookService.getBook(id).subscribe(
      book => { this.book = book },
      error => { this.error = error.message },
    );
  }

  ngOnInit() {
  }

}
