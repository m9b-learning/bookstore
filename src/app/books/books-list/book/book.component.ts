import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Book } from 'src/app/models/book.model';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss']
})
export class BookComponent implements OnInit {
  @Input() book: Book;
  @Output() notify: EventEmitter<number> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  onClick() {
    console.log('notify', this.book.id);
    this.notify.emit(this.book.id);
  }

}
