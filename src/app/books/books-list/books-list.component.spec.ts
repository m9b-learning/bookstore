import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { of } from 'rxjs';

import { BooksListComponent } from './books-list.component';
import { BookService } from 'src/app/services/book.service';
import { TestModule } from 'src/app/test/test.module';

describe('BooksListComponent', () => {
  let component: BooksListComponent;
  let fixture: ComponentFixture<BooksListComponent>;

  const mockBookService = jasmine.createSpyObj('BookService', {
    getBooks: of([{
      id: 0,
      title: 'Title 1',
      author: 'Author 1'
    }, {
      id: 1,
      title: 'Title 2',
      author: 'Author 2'
    }])
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BooksListComponent ],
      schemas: [ NO_ERRORS_SCHEMA ],
      providers: [
        { provide: BookService, useValue: mockBookService }
      ],
      imports: [
        TestModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BooksListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create 2 app-book components', () => {
    const el = fixture.nativeElement;
    expect(el.querySelectorAll('app-book').length).toEqual(2);
  });
});
