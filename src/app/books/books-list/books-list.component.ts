import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { Book } from 'src/app/models/book.model';
import { BookService } from 'src/app/services/book.service';

@Component({
  selector: 'app-books-list',
  templateUrl: './books-list.component.html',
  styleUrls: ['./books-list.component.scss']
})
export class BooksListComponent implements OnInit {
  @Output() alert = new EventEmitter();
  
  private _value = 0;

  get value() {
    return this._value;
  }
  
  set value(val: number) {
    this._value = val;
    this.alert.emit(val);
  }

  books: Book[];
  error: string;

  constructor(
    private bookService: BookService
  ) {
    this.bookService.getBooks().subscribe(
      (books: Book[]) => { this.books = books },
      (error) => { this.error = error.message; }
    );
  }

  ngOnInit() {
    setInterval(() => {
      this.value++;
    }, 1000);
  }

  onEvent(identifier: number) {
    console.log('BooksListComponent onEvent', identifier);
  }
}
