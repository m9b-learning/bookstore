import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { BooksListComponent } from './books-list/books-list.component';
import { ToolsModule } from '../tools/tools.module';
import { BookComponent } from './books-list/book/book.component';
import { BookDetailComponent } from './book-detail/book-detail.component';
import { BooksComponent } from './books.component';
import { booksRoutes } from './books.routes';

@NgModule({
  declarations: [
    BooksListComponent,
    BookComponent,
    BookDetailComponent,
    BooksComponent
  ],
  imports: [
    CommonModule,
    ToolsModule,
    RouterModule.forChild(booksRoutes)
  ],
  exports: [
    BooksListComponent
  ]
})
export class BooksModule { }
