import { Routes } from '@angular/router';

import { BooksListComponent } from './books-list/books-list.component';
import { BookDetailComponent } from './book-detail/book-detail.component';
import { BooksComponent } from './books.component';

export const booksRoutes: Routes = [{ 
    path: 'books',
    component: BooksComponent ,
    children: [
      { path: ':id', component: BookDetailComponent },
      { path: '', component: BooksListComponent }    
    ]
  }];
