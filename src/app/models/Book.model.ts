export class Book {

  public id?: number;

  constructor(
    public title?: string,
    public author?: string,
    public resume?: string
  ) { }
}
