import { TestBed } from '@angular/core/testing';

import { AuthorService } from './author.service';
import { of } from 'rxjs';
import { HttpClient } from '@angular/common/http';

describe('AuthorService', () => {

  const mockHttpClient = jasmine.createSpyObj('HttpClient', {
    get: of([{
      id: 0,
      name: 'Author 1'
    }, {
      id: 1,
      name: 'Author 2'
    }])
  });
  
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      { provide: HttpClient, useValue: mockHttpClient }
    ]
  }));

  it('should be created', () => {
    const service: AuthorService = TestBed.get(AuthorService);
    expect(service).toBeTruthy();
  });
});
