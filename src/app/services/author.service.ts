import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Author } from 'src/app/models/author.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthorService {

  endPoint = `${environment.baseUrl}/authors`;

  constructor(private http: HttpClient) { 
  }

  getAuthors(): Observable<Author[]> {
    return this.http.get<Author[]>(this.endPoint);    
  }

  createAuthor(author: Author): Observable<Author> {
    return this.http.post<Author>(this.endPoint, author);
  }
}
