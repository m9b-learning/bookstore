import { TestBed } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';

import { BookService } from './book.service';

describe('BookService', () => {

  const mockHttpClient = jasmine.createSpyObj('HttpClient', {
    get: of([{
      id: 0,
      title: 'Title 1',
      author: 'Author 1'
    }, {
      id: 1,
      title: 'Title 2',
      author: 'Author 2'
    }])
  });
  
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      { provide: HttpClient, useValue: mockHttpClient }
    ]
  }));

  it('should be created', () => {
    const service: BookService = TestBed.get(BookService);
    expect(service).toBeTruthy();
  });
});
