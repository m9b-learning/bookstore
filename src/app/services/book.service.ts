import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Book } from '../models/book.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  endPoint = `${environment.baseUrl}/books`;

  constructor(private http: HttpClient) { 
  }

  getBook(id: number): Observable<Book> {
    return this.http.get<Book>(`${this.endPoint}/${id}`);
  }

  getBooks(): Observable<Book[]> {
    return this.http.get<Book[]>(this.endPoint);    
  }

  createBook(book: Book): Observable<Book> {
    return this.http.post<Book>(this.endPoint, book);
  }
}
