import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterTestingModule,
    HttpClientTestingModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule.forRoot()
  ],
  exports: [
    RouterTestingModule,
    HttpClientTestingModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule
  ]
})
export class TestModule { }
