import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

import { UppercaseDirective } from './uppercase.directive';
import { FormErrorComponent } from './form-error/form-error.component';

@NgModule({
  declarations: [
    UppercaseDirective,
    FormErrorComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule.forChild()
  ],
  exports: [
    UppercaseDirective,
    HttpClientModule,
    FormErrorComponent,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule
  ]
})
export class ToolsModule { }
